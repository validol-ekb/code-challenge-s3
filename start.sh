#!/usr/bin/env bash

exec java \
    -XX:NativeMemoryTracking=summary \
    -XshowSettings:vm \
    -Dconfig.resource=application-dev.conf \
    -jar /app/assembly.jar
