package com.eyeem.photos

import akka.stream.{ ActorMaterializer, Materializer }
import org.scalatest.BeforeAndAfterAll
import org.scalatest.concurrent.{ ScalaFutures, Waiters }
import org.scalatest.time.{ Millis, Seconds, Span }
import org.scalatestplus.play.PlaySpec
import play.api.libs.ws.ahc.AhcWSClient

abstract class BaseSpec extends PlaySpec with ScalaFutures with Waiters with NamedLogger with BeforeAndAfterAll {

  implicit override val patienceConfig =
    PatienceConfig(timeout = Span(30, Seconds), interval = Span(50, Millis))

  implicit val actorSystem = akka.actor.ActorSystem()

  implicit lazy val materializer: Materializer = ActorMaterializer()(actorSystem)

  implicit val wsClient = AhcWSClient()(materializer)

  override def afterAll() = {
    wsClient.close()
  }

}
