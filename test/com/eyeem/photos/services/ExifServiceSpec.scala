package com.eyeem.photos.services

import com.eyeem.photos.config.ExifConfig
import org.scalatest.mockito.MockitoSugar
import org.scalatest.{ FreeSpec, Matchers }
import play.api.libs.ws.{ WSClient, WSRequest }

import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import org.mockito.Mockito._
import play.api.libs.json.JsValue

import scala.concurrent._
import org.scalatest.concurrent._

class ExifServiceSpec extends FreeSpec with Matchers with MockitoSugar with ScalaFutures {

  private val config = ExifConfig("http://localhost/", 2.seconds, 1)
  private val id = "12345"

  "ExifService" - {

    "should return Future.success(Some(json)) on valid response" in {
      withMocks { (clientMock, wsRequestMock) =>
        val jsonMock = mock[JsValue]
        val wsResponseMock = mock[wsRequestMock.Response]

        when(wsRequestMock.get()).thenReturn(Future.successful(wsResponseMock))
        when(wsResponseMock.status).thenReturn(200)
        when(wsResponseMock.json).thenReturn(jsonMock)

        val exifService = ExifService(config, clientMock)

        whenReady(exifService.getInfo(id)) { response =>
          response shouldEqual Some(jsonMock)
        }
      }
    }

    "should return Future.success(None) on NotFound response" in {
      withMocks { (clientMock, wsRequestMock) =>
        val wsResponseMock = mock[wsRequestMock.Response]

        when(wsRequestMock.get()).thenReturn(Future.successful(wsResponseMock))
        when(wsResponseMock.status).thenReturn(404)
        val exifService = ExifService(config, clientMock)

        whenReady(exifService.getInfo(id)) { response =>
          response shouldEqual None
        }
      }
    }

    "should return Future.success(None) immediately on invalid http status and 0 retries at config" in {
      withMocks { (clientMock, wsRequestMock) =>
        val wsResponseMock = mock[wsRequestMock.Response]

        when(wsRequestMock.get()).thenReturn(Future.successful(wsResponseMock))
        when(wsResponseMock.status).thenReturn(500)
        val exifService = ExifService(config.copy(retries = 0), clientMock)

        whenReady(exifService.getInfo(id)) { response =>
          response shouldEqual None
        }
      }
    }

    "should retry failed request once and then return Future.success(None)" in {
      withMocks { (clientMock, wsRequestMock) =>
        val wsResponseMock = mock[wsRequestMock.Response]

        when(wsRequestMock.get()).thenReturn(Future.successful(wsResponseMock))
        when(wsResponseMock.status).thenReturn(500)
        val exifService = ExifService(config, clientMock)

        whenReady(exifService.getInfo(id)) { response =>
          verify(wsRequestMock, times(2)).get()
          response shouldEqual None
        }
      }

    }

    "should retry failed request once and then return Future.success(Some(json))" in {
      withMocks { (clientMock, wsRequestMock) =>
        val wsResponseMock = mock[wsRequestMock.Response]
        val jsonMock = mock[JsValue]

        when(wsRequestMock.get()).thenReturn(Future.successful(wsResponseMock))
        when(wsResponseMock.status).thenReturn(500).thenReturn(200)
        when(wsResponseMock.json).thenReturn(jsonMock)
        val exifService = ExifService(config, clientMock)

        whenReady(exifService.getInfo(id)) { response =>
          verify(wsRequestMock, times(2)).get()
          verify(wsResponseMock, times(2)).status
          verify(wsResponseMock, times(1)).json
          response shouldEqual Some(jsonMock)
        }
      }
    }

    "should retry request which was terminated when timeout is exceeded" in {
      withMocks { (clientMock, wsRequestMock) =>
        val wsResponseMock = mock[wsRequestMock.Response]
        val jsonMock = mock[JsValue]

        when(wsRequestMock.get())
          .thenReturn(Future.failed(new TimeoutException("Time limit is reached")))
          .thenReturn(Future.successful(wsResponseMock))

        when(wsResponseMock.status).thenReturn(200)
        when(wsResponseMock.json).thenReturn(jsonMock)
        val exifService = ExifService(config, clientMock)

        whenReady(exifService.getInfo(id)) { response =>
          verify(wsRequestMock, times(2)).get()
          verify(wsResponseMock, times(1)).status
          verify(wsResponseMock, times(1)).json
          response shouldEqual Some(jsonMock)
        }
      }
    }

    "should return Future.failed if something went totally wrong" in {
      withMocks { (clientMock, wsRequestMock) =>
        when(wsRequestMock.get())
          .thenReturn(Future.failed(new OutOfMemoryError("error")))

        val exifService = ExifService(config, clientMock)
        whenReady(exifService.getInfo(id).failed) { error =>
          error shouldBe an[ExecutionException]
        }
      }
    }
  }

  private def withMocks[T](fn: (WSClient, WSRequest) => T): T = {
    val clientMock = mock[WSClient]
    val wsRequestMock = mock[WSRequest]

    when(clientMock.url(s"http://localhost/$id")).thenReturn(wsRequestMock)
    when(wsRequestMock.addHttpHeaders("Accept" -> "application/json")).thenReturn(wsRequestMock)
    when(wsRequestMock.withRequestTimeout(2.seconds)).thenReturn(wsRequestMock)
    fn(clientMock, wsRequestMock)
  }

}
