package com.eyeem.photos.controllers

import com.eyeem.photos.controllers.MockComponents._
import com.eyeem.photos.model.Photo
import org.scalatestplus.play.PlaySpec
import play.api.ApplicationLoader
import org.mockito.Mockito._
import play.api.libs.json.{ JsNumber, JsObject, Json }
import play.api.test.FakeRequest
import play.api.test.Helpers.{ GET, route, status }
import play.api.test.Helpers._
import org.mockito.ArgumentMatchers.{ eq => mockitoEq, _ }

import scala.concurrent.{ ExecutionContext, Future }

class PhotosSpec extends PlaySpec with OneAppPerTestWithComponents[TestComponents] {

  override def createComponents(context: ApplicationLoader.Context): TestComponents = new TestComponents(context)

  private val id = "12345"
  private val photo = Photo(
    id = id,
    description = "",
    username = "",
    exif = None,
    url = None
  )

  "PhotosController" must {

    "getPhoto" must {

      "should return photo by id with mixed exif data" in {
        val exif = JsObject(Map("exposureTime" -> JsNumber(500)))
        val photoWithExif = photo.copy(exif = Some(exif))
        when(mockPhotoService.getInfo(mockitoEq(id))(any[ExecutionContext])).thenReturn(Future.successful(Some(photo)))
        when(mockExifService.getInfo(id)).thenReturn(Future.successful(Some(exif)))

        val result = route(app, FakeRequest(GET, s"/photo/$id")).get
        status(result) mustEqual OK
        contentAsString(result) mustEqual Json.toJson(photoWithExif).toString()
      }

      "should return photo by id without mixed exif data" in {
        when(mockPhotoService.getInfo(mockitoEq(id))(any[ExecutionContext])).thenReturn(Future.successful(Some(photo)))
        when(mockExifService.getInfo(id)).thenReturn(Future.successful(None))

        val result = route(app, FakeRequest(GET, s"/photo/$id")).get
        status(result) mustEqual OK
        contentAsString(result) mustEqual Json.toJson(photo).toString()
      }

      "should return NotFound if there isn't any photo" in {
        when(mockPhotoService.getInfo(mockitoEq(id))(any[ExecutionContext])).thenReturn(Future.successful(None))
        when(mockExifService.getInfo(id)).thenReturn(Future.successful(None))

        val result = route(app, FakeRequest(GET, s"/photo/$id")).get
        status(result) mustEqual NOT_FOUND
      }

      "should return InternalServerError if photo service returned failure" in {
        when(mockPhotoService.getInfo(mockitoEq(id))(any[ExecutionContext])).thenReturn(Future.failed(new Error))
        when(mockExifService.getInfo(id)).thenReturn(Future.successful(None))

        val result = route(app, FakeRequest(GET, s"/photo/$id")).get
        status(result) mustEqual INTERNAL_SERVER_ERROR
      }

      "should return InternalServerError if exif service returned failure" in {
        when(mockPhotoService.getInfo(mockitoEq(id))(any[ExecutionContext])).thenReturn(Future.successful(None))
        when(mockExifService.getInfo(id)).thenReturn(Future.failed(new Error))

        val result = route(app, FakeRequest(GET, s"/photo/$id")).get
        status(result) mustEqual INTERNAL_SERVER_ERROR
      }

      "should return InternalServerError if both services returned failure" in {
        when(mockPhotoService.getInfo(mockitoEq(id))(any[ExecutionContext])).thenReturn(Future.failed(new Error))
        when(mockExifService.getInfo(id)).thenReturn(Future.failed(new Error))

        val result = route(app, FakeRequest(GET, s"/photo/$id")).get
        status(result) mustEqual INTERNAL_SERVER_ERROR
      }
    }

  }
}
