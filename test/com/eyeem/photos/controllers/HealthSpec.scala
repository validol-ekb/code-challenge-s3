package com.eyeem.photos.controllers

import com.eyeem.photos.controllers.MockComponents._
import org.mockito.Mockito._
import org.scalatestplus.play.PlaySpec
import play.api.ApplicationLoader.Context
import play.api.test.FakeRequest
import play.api.test.Helpers._

import scala.concurrent.Future

class HealthSpec extends PlaySpec with OneAppPerTestWithComponents[TestComponents] {

  override def createComponents(context: Context) = new TestComponents(context)

  "Health Controller" must {

    "have a health endpoint" in {

      when(mockUpService.up()).thenReturn(Future.successful("DOWN"))

      val result = route(app, FakeRequest(GET, "/health")).get
      status(result) mustEqual SERVICE_UNAVAILABLE
      contentAsString(result) must include("DOWN")
    }

  }

}
