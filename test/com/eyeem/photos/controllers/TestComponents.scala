package com.eyeem.photos.controllers

import com.eyeem.photos.PhotosComponents
import com.eyeem.photos.services.{ ExifService, PhotoService, UpService }
import org.scalatest.mockito.MockitoSugar
import play.api.ApplicationLoader.Context

object MockComponents extends MockitoSugar {
  val mockUpService = mock[UpService]
  val mockPhotoService = mock[PhotoService]
  val mockExifService = mock[ExifService]
}

class TestComponents(context: Context) extends PhotosComponents(context) {
  override lazy val upService = MockComponents.mockUpService
  override lazy val photoService = MockComponents.mockPhotoService
  override lazy val exifService = MockComponents.mockExifService
}
