# Tagging system architecture

## General information 
So, current system is using AWS S3 for storing all the information. AWS provides some basic tag functionality
but there is a big disadvantage - there isn't explicit search functionality. We can try to come up with some exotic solution with 
AWS-credentials and create user for every tag, so it can see only files with special tag, but I'm sure that 
isn't a good idea, because it is too complicated and actually it's hack and won't be working properly. I think 
the best solution for such purposes would be [Elasticsearch](https://www.elastic.co/).

## Saving photo algorithm
First of all we should add tag(s) field in json object. Our current format looks like

```json
{
  "username": 123456789,
  "description": "On a sunny day"
}
```

I don't like complicated solutions, that's why I think the best way to add tags would be like this

```json
{
  "username": 123456789,
  "description": "On a sunny day",
  "tags": ["nature", "landscape", "travelling"]
}
```
Users add tags by themselves, I think the only thing that we can do is auto completion which is based on already 
existing tags, for example like tags on [stackoverflow.com](https://stackoverflow.com).

Next step would be saving on server side. We need to implement validation system, because users add tags by themselves, 
they can add something not really polite. It's quite common problem, so I just skip it, because there are lots of ready solution.

After validation, if everything is fine we have to save photo. On first step we save image to S3 and retrieve all meta 
information from there. Then we need to put this meta information (not all of course) to Elasticsearch. I see index 
structure in such way

```json
{
  "id": "a6e2a06b61626cd90c43255767775be1",
  "description": "On a sunny day",
  "username": 123456789,
  "url": "http://s3-website-eu-west-1.amazonaws.com/thumbnail/64x64/a6e2a06b61626cd90c43255767775be1.jpg",
  "tags": ["nature", "landscape", "travelling"]
}
```
Some words about fields
* id - we need it just for direct linking of elasticsearch document with file in S3
* description, username - optional fields. I added them only for searching purposes, if we don't have further plans to 
search within these fields we can throw them away from the index.
* url - this field is needed for displaying search page, in general it should be a preview url. Main idea - it's to maintain 
searching only by querying to Elasticsearch without additional queries to S3.
* tags - simple array of strings. Elasticsearch creates index for such fields by default, so searching will work really 
fast with only one query to Elasticsearch. Request query to Elasticsearch would be looking like that
```json
{
  "query": {
    "match": {
      "tags": "travelling"
    }
  }
}
```

## Searching
So, searching endpoints would be really simple. I think there isn't any reason to do separated endpoints for one tag and for several tags. 
We need just to implement several tags endpoint. So query would be like this

```
GET /photos/search?tag=nature,landscape,travelling
```
Server will receive this query and just transform to Elasticsearch query like this
```json
{
  "query": {
    "bool": {
      "should": [
        {
          "match": {
            "tags": "nature"
          }
        },
        {
          "match": {
            "tags": "landscape"
          }
        },
        {
          "match": {
            "tags": "travelling"
          }
        }
      ]
    }
  }
}
```
Also we should add pagination for response due to avoid long queries with lots of data. After response receiving we should 
transform elasticsearch response to internal models, main goal as I said is avoiding sub-queries to S3 and maintaining search queries
only by queries to Elasticsearch, that's why we need to store some additional fields in the index.

## Advantages
* Simple on client application side.
* It works really fast. I have experience with this technology and I can say that it's the fastest ready solution   
* Scalability. It supports clustering from the box, so we can scale it for several instances. Also it supports sharding 
and we can split big index into several smaller indexes.

## Disadvantages
* We should support additional system. We need to update index and keep it in actual state.
* Denormalization and additional space usage.
* In model which I described we will have some difficulties with tag deleting. If we decide to delete one tag we will 
need to find all documents with this tag and update it in general it's quite heavy operation.
* Non-atomic mutable operations, because we need to create/update data in several systems there is a possibility of 
non-consistent states within our service.





