
name := """eyeem-photos"""

lazy val root = (project in file("."))
  .enablePlugins(sbtdocker.DockerPlugin)
  .enablePlugins(PlayScala)
  .enablePlugins(DebianPlugin)
  .enablePlugins(JavaAppPackaging)
  .configs(CustomIntegrationTest)
  .settings(inConfig(CustomIntegrationTest)(Defaults.testTasks): _*)

scalaVersion := "2.12.6"

libraryDependencies ++= Seq(
  cache,
  ws,

  "com.softwaremill.macwire" %% "macros" % "2.3.0" % "provided",
  "com.softwaremill.macwire" %% "util" % "2.3.0",
  "com.github.melrief" %% "pureconfig" % "0.6.0",
  "com.lightbend.akka" %% "akka-stream-alpakka-s3" % "1.0-M1",
  "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.2" % Test,
  "org.mockito" % "mockito-core" % "2.13.0" % Test
)

excludeDependencies += "commons-logging" % "commons-logging"

resolvers += Resolver.bintrayRepo("eyeem", "maven-private")
resolvers += Resolver.bintrayRepo("eyeem", "maven")

//credentials += Credentials(Path.userHome / ".bintray" / ".credentials-read")

scalacOptions ++= Seq(
  "-deprecation", // Emit warning and location for usages of deprecated APIs.
  "-feature", // Emit warning and location for usages of features that should be imported explicitly.
  "-unchecked", // Enable additional warnings where generated code depends on assumptions.
  "-Xlint", // Enable recommended additional warnings.
  "-Ywarn-adapted-args", // Warn if an argument list is modified to match the receiver.
  "-Ywarn-dead-code", // Warn when dead code is identified.
  "-Ywarn-inaccessible", // Warn about inaccessible types in method signatures.
  "-Ywarn-nullary-override", // Warn when non-nullary overrides nullary, e.g. def foo() over def foo.
  "-Ywarn-numeric-widen", // Warn when numerics are widened.
  "-language:implicitConversions", //allow implicit convertions defined by implicit def convertAtoB(a:A):B type functions
  "-language:postfixOps" // allow writing e.g. `5 seconds` with a space in between
)

mainClass in assembly := Some("play.core.server.ProdServerStart")
fullClasspath in assembly += Attributed.blank(PlayKeys.playPackageAssets.value)

assemblyMergeStrategy in assembly := {
  case x if x.endsWith("reference-overrides.conf") => MergeStrategy.concat
  case x                                           => (assemblyMergeStrategy in assembly).value(x)
}

dockerfile in docker := {
  new Dockerfile {
    from("anapsix/alpine-java:8u202b08_jdk")
    copy(assembly.value, "/app/assembly.jar")
    copy(file("./start.sh"), "/app/start.sh")
    run("chmod", "+x", "/app/start.sh")
    expose(9000)
    cmd("/app/start.sh")
  }
}

//********************************************************
// Test settings
//********************************************************

def integrationFilter(name: String): Boolean = name endsWith "ITSpec"

def unitFilter(name: String): Boolean = {
  (name endsWith "Spec") && !integrationFilter(name)
}

lazy val CustomIntegrationTest = config("it") extend Test

testOptions in Test += Tests.Argument("-oF") //show full stack traces
testOptions in CustomIntegrationTest += Tests.Argument("-oF") //show full stack traces

testOptions in Test := Seq(Tests.Filter(unitFilter))
testOptions in CustomIntegrationTest := Seq(Tests.Filter(integrationFilter))

//********************************************************
// Debian Settings
//********************************************************
lazy val gitHeadCommitSha = Process(
  "git rev-parse --short HEAD").lines.head

lazy val date: String = Process("date +\"%Y-%m-%dT%H-%M\"").lines.head.replace("\"", "")
version in GlobalScope := "0.1.0" + "-" + date + "-gitSha-" + gitHeadCommitSha
maintainer in Linux := "Infra <infra@eyeem.com>"
packageSummary in Linux := "photos"
packageDescription := "photos"

