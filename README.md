Hi!

Thanks for taking time to complete our backend engineering work task. It consists of three parts
* An engineering challenge 
* A modeling question
* A bonus task

Base rules: keep the code quality in mind, document where necessary, test where applicable.

## 1) The coding task

Implement a second endpoint on a small microservice offering an API supporting:
 * File upload functionality & business logic (already existing)
 * Data retrieval with composing a response with data from another downstream service.
 
We've already prepared a scaffold for you - consisting of a Scala app utilizing the Play framework. You can pick any further libraries of your choice while adhering to the KISS principle. You are not expected to consider any form of caching, authentication or performance optimization, but we'd like to see at least one test :)

Overall, correct implementation has more impact than time needed. Please include a small README how to run your code.

### Flow overview
```
Frontend (don't implement)          PhotoService
========                            ================================
Takes in data
Submit            POST /photos/ =>  - checks the photo                 (already implemented)
                                    - generates photo ID
                                    - stores photo on S3
                                    - assembles response
                 <= Returns JSON
                 
                 
 Loads photo      GET /photo/:id    - checks photo for availability     (to be implemented)
                                    - queries for EXIF data
                                    - assembles response
                 <= Returns JSON            
```

### The photo service API

###### POST /photo/ (already implemented)
Takes in a photo and some metadata, uploads the photo to S3 and gives a response. **The endpoint checks whether a certain photo already has been uploaded to the platform already.** If this case is encountered, the upload request is rejected.
Example request: 
```
{
  "user": 123456789,
  "description": "On a sunny day"
}
```
...plus photo attached as multipart data (photo data key: ‘photo’, json data key: ‘json’). 

Possible response codes
* `HTTP 201 CREATED`
* `HTTP 409 CONFLICT` The exact same photo already has been uploaded before
* `HTTP 500 SERVER ERROR` (e.g. on upload error or similar)

Example response:
```
{
  "id": 1,
  "user": 123456789,
  "description": "On a sunny day",
  "url": "http://eyeem-code-challenge.s3-website-eu-west-1.amazonaws.com/code-challenge-<firstname>/<photoID>.jpg"
}
```

###### GET /photo/:id (to be implemented)
Retrieves photo metadata from different sources and displays them.

Possible response codes
* `HTTP 200 OK`
* `HTTP 404 NOT FOUND` The exact same photo already has been uploaded before

Example response:
```
{
  "id": 1,
  "user": 123456789,
  "description": "On a sunny day",
  "url": "http://eyeem-code-challenge.s3-website-eu-west-1.amazonaws.com/code-challenge-<firstname>/<photoID>.jpg" ,
  "exif": {
    "dateTime": "2009-01-01T12:00:00+01:00",
    "exposureTime": 0.5,
    "fNumber": 2.0,
    "orientation": 0
  }
}
```

To retrieve the EXIF data needed for the GET /photo/ endpoint we have created a mock endpoint which will generate **random** EXIF data on each request. The endpoint can be reached via 
```
https://xl9l9t91ik.execute-api.eu-west-1.amazonaws.com/prod/exif/<photoId>
```
and will return a JSON response. 
Possible response codes

* `HTTP 200 OK`
* `HTTP 404 NOT FOUND`
* `HTTP 429 TOO MANY REQUESTS`

When reporting back photo information on the backend do a concurrent call to the EXIF service to gather (possible) EXIF data. If you encounter a positive response, i.e. a `200 OK`, incorporate the EXIF data into the upload response. On a non-positive answer, basic sensible fallback behaviour should be implemented by you.

### Helpful resources

#### Credentials

You will find the AWS credentials to access S3 in the email we sent to you with this task.
You have RW access in a folder in the S3 bucket to upload (we already created it for you):
```
s3://eyeem-code-challenge/code-challenge-<firstname>/
```

#### Docs

##### Dealing with Amazon S3

## 2) Modeling/Architecture Questions (no coding required)

The community wants us to introduce **tags** when uploading photos.

* Please remodel the existing endpoint to take in a list of tags, define the new JSON request format.
* Design an endpoint to query all photos which are associated with a certain tag, note down the request/response behaviour.
* Give an outline on how you would model the data model/storage layer of storing tags and being able to query all photos related to a tag in a performant manner.

Please provide the free-form/text responses to your questions in the `ANSWERS.md` in this repository.

## 3) Bonus Task (optional)

Can you dockerize the app?


Have fun & thanks again! If you have any other questions regarding the challenge do not hesitate to write us via `ce-platform@eyeem.com` - an engineer will help you out!

## Comments to solution

## 1) The coding task
* I moved all user-specific config parameters to `application-dev.conf` due this reason you should run application with such command
```
sbt -Dconfig.resource=application-dev.conf run
```
* Fallback strategy. I decided to do optimistic scenario. It means that we are trying to avoid failure responses on user side as much as we can.
That's why the only possible situation for failure response - problems with file retrieving from S3 bucket. Second downstream service is a source 
of additional information and its failure doesn't lead to clients request failure. if it fails we won't see EXIF information only. Also I did request
retrying in ExifService, it provides simple retrying logic in case of some short-time network problems or other NonFatal errors.

### Example of usage
Lets call a photo information endpoint for photo with id `aba534699106bde4e717dc917511e1e6` (I've already uploaded it during testing). 
Request looks like  
```bash
$ curl http://localhost:9000/photo/aba534699106bde4e717dc917511e1e6
```
Response could look in 2 ways:
1. If EXIF mock service returned 200 OK message it would be
```json
{
  "id":"aba534699106bde4e717dc917511e1e6",
  "description":"On a sunny day",
  "username":"123456789",
  "exif": {
    "dateTime":"2011-11-22T20:46:43.286551+00:00",
    "exposureTime":375,
    "fNumber":9.9,
    "orientation":1,
  },
  "url":"http://eyeem-code-challenge.s3-website-eu-west-1.amazonaws.com/code-challenge-valeriy/aba534699106bde4e717dc917511e1e6.jpg",
}
```
2. if EXIF mock service returned an error message it would be
```json
{
  "id":"aba534699106bde4e717dc917511e1e6",
  "description":"On a sunny day",
  "username":"123456789",
  "url":"http://eyeem-code-challenge.s3-website-eu-west-1.amazonaws.com/code-challenge-valeriy/aba534699106bde4e717dc917511e1e6.jpg",
}
```
If there is no photo with id server will return response with 404 status

## 2) Modeling/Architecture Questions

[Here are answers](https://github.com/eyeem/code-challenge-valeriy/blob/master/ANSWERS.md)

## 3) Bonus Task
I dockerized application with [sbt-docker](https://github.com/marcuslonnberg/sbt-docker) plugin. I think it's the simliest way to do it.
Here is the `build.sbt` code snippet
```sbtshell
dockerfile in docker := {
  new Dockerfile {
    from("anapsix/alpine-java:8u202b08_jdk")
    copy(assembly.value, "/app/assembly.jar")
    copy(file("./start.sh"), "/app/start.sh")
    run("chmod", "+x", "/app/start.sh")
    expose(9000)
    cmd("/app/start.sh")
  }
}
```
As a base image I chose Alpine Linux with pre-installed openjdk8 because it's one of the lightest docker images. I did a simple bash script which runs app within container
```bash
exec java \
    -XX:NativeMemoryTracking=summary \
    -XshowSettings:vm \
    -Dconfig.resource=application-dev.conf \
    -jar /app/assembly.jar
```
Of course it isn't a production solution because config with credentials is hardcoded here. But I did it especially due
convenience reasons. For production it's better to pass credentials via environment variables or via password manages
such as [gopass](https://github.com/gopasspw/gopass) or some AWS solutions.
You can easily build and run application with commands
```bash
$ sbt docker
$ docker-compose up
```
if you don't have `docker-compose` you can run it with `docker`
```bash
$ docker run -it --rm --name eyeem-photos -p 9000:9000 eyeem-photos/eyeem-photos
```