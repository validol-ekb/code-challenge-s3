package com.eyeem.photos.config

case class AwsConfig(accessKey: String, secretKey: String, folder: String, bucket: String, region: String)
