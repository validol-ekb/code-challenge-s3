package com.eyeem.photos.config

import scala.concurrent.duration.Duration

case class ExifConfig(url: String, timeout: Duration, retries: Int)
