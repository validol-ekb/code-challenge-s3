package com.eyeem.photos

import org.slf4j.LoggerFactory

trait NamedLogger {
  val log = LoggerFactory.getLogger(this.getClass)
}
