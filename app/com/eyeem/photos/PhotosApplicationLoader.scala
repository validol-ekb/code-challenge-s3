package com.eyeem.photos

import _root_.controllers.AssetsComponents
import akka.stream.Materializer
import akka.stream.alpakka.s3.{ MemoryBufferType, S3Settings }
import akka.stream.alpakka.s3.impl.ListBucketVersion2
import akka.stream.alpakka.s3.scaladsl.S3Client
import com.amazonaws.auth.{ AWSStaticCredentialsProvider, BasicAWSCredentials }
import com.amazonaws.regions.AwsRegionProvider
import com.eyeem.photos.config.{ AwsConfig, ExifConfig }
import com.eyeem.photos.controllers.{ HealthController, PhotosController }
import com.eyeem.photos.repositories._
import com.eyeem.photos.services.{ ExifService, HealthIndicating, PhotoService, UpService }
import com.softwaremill.macwire._
import play.api.ApplicationLoader.Context
import play.api._
import play.api.i18n._
import play.api.libs.ws.ahc.AhcWSComponents
import play.api.mvc._
import play.api.routing.Router
import router.Routes

import scala.concurrent.Future

class PhotosApplicationLoader extends ApplicationLoader {
  def load(context: Context): Application = new PhotosComponents(context).application
}

class PhotosComponents(context: Context) extends BuiltInComponentsFromContext(context)
    with PhotosModule
    with I18nComponents
    with AhcWSComponents
    with AssetsComponents {

  // set up logger
  LoggerConfigurator(context.environment.classLoader).foreach {
    _.configure(context.environment)
  }

  lazy val router: Router = {
    // add the prefix string in local scope for the Routes constructor
    val prefix: String = "/"
    wire[Routes]
  }

  override def httpFilters: Seq[EssentialFilter] = Seq(new NoopFilter)
}

class NoopFilter(implicit val mat: Materializer) extends Filter {
  override def apply(f: RequestHeader => Future[Result])(rh: RequestHeader): Future[Result] = f(rh)
}

trait PhotosModule {

  //cake me the play executioncontext, actorsystem and materializer and wsClient
  this: BuiltInComponents with AhcWSComponents =>

  def controllerComponents: ControllerComponents

  import pureconfig._
  implicit def hint[T]: ProductHint[T] = ProductHint[T](ConfigFieldMapping(CamelCase, CamelCase))

  lazy val awsConfig: AwsConfig = loadConfigOrThrow[AwsConfig]("aws")

  lazy val photoRepository: S3PhotoRepository = {
    val awsCredentialsProvider = new AWSStaticCredentialsProvider(new BasicAWSCredentials(awsConfig.accessKey, awsConfig.secretKey))
    val regionProvider = new AwsRegionProvider {
      def getRegion: String = awsConfig.region
    }

    val settings = new S3Settings(MemoryBufferType, None, awsCredentialsProvider, regionProvider, false, None, ListBucketVersion2)
    val s3Client = new S3Client(settings)(actorSystem, materializer)

    new S3PhotoRepository(
      folder = awsConfig.folder,
      bucket = awsConfig.bucket,
      s3client = new S3ClientWrapper(s3Client),
      region = awsConfig.region
    )
  }

  lazy val exifService: ExifService = ExifService(loadConfigOrThrow[ExifConfig]("exif"), wsClient)(executionContext)

  lazy val healthIndicators: Seq[HealthIndicating] = Seq(
    photoRepository
  )

  lazy val photoService: PhotoService = wire[PhotoService]
  lazy val upService: UpService = wire[UpService]
  lazy val healthController: HealthController = wire[HealthController]
  lazy val photoController: PhotosController = wire[PhotosController]

}
