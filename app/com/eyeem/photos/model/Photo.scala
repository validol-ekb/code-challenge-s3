package com.eyeem.photos.model

import play.api.libs.json.{ JsValue, Json, Writes }

object Photo {
  implicit val jsonWrites: Writes[Photo] = Json.writes[Photo]
}
case class Photo(id: String, description: String, username: String, exif: Option[JsValue] = None, url: Option[String] = None)
