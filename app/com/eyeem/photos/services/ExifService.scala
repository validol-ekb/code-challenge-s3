package com.eyeem.photos.services

import akka.http.scaladsl.model._
import com.eyeem.photos.NamedLogger
import com.eyeem.photos.config.ExifConfig
import com.eyeem.photos.services.ExifService.FailedRequestException
import play.api.libs.json.JsValue
import play.api.libs.ws.WSClient

import scala.concurrent.{ ExecutionContext, ExecutionException, Future }
import scala.util.control.NonFatal

class ExifService(config: ExifConfig, wsClient: WSClient)(implicit ec: ExecutionContext) extends NamedLogger {

  private val url = config.url.stripSuffix("/").trim

  def getInfo(id: String): Future[Option[JsValue]] = {
    request(config.retries, id)
  }

  private def request(retries: Int, id: String): Future[Option[JsValue]] = {
    wsClient
      .url(s"$url/$id")
      .addHttpHeaders("Accept" -> "application/json")
      .withRequestTimeout(config.timeout)
      .get()
      .map { response =>
        response.status match {
          case StatusCodes.OK.intValue => Some(response.json)
          case StatusCodes.NotFound.intValue => None
          case other =>
            throw FailedRequestException(other)
        }
      }.recoverWith {
        case e: ExecutionException =>
          log.error(s"Failed EXIF request for $id", e.getCause)
          Future.failed(e.getCause)
        case NonFatal(e) if retries != 0 =>
          val left = retries - 1
          log.warn(s"Exception during EXIF service request for $id trying to retry, retries left $left", e)
          request(left, id)
        case NonFatal(e) =>
          log.error(s"Failed EXIF request for $id", e)
          Future.successful(None)
      }
  }
}

object ExifService {

  case class FailedRequestException(code: Int) extends Exception(s"Response code ${code.toString}")

  def apply(config: ExifConfig, wsClient: WSClient)(implicit ec: ExecutionContext): ExifService = {
    new ExifService(config, wsClient)
  }

}
