package com.eyeem.photos.services

import scala.concurrent.{ ExecutionContext, Future }

/**
 * A service that implement this trait can indicate if it is healthy, e.g. it reaches its upstream/downstream service.
 */
trait HealthIndicating {
  /**
   *
   * @return true => healthy
   */
  def healthy()(implicit ec: ExecutionContext): Future[Boolean]
}