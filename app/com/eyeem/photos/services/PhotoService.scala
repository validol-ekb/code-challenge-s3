package com.eyeem.photos.services

import akka.stream.scaladsl.Source
import akka.util.ByteString
import com.eyeem.photos.model.Photo
import com.eyeem.photos.repositories.PhotoRepository
import com.eyeem.photos.repositories.PhotoRepository.Failure

import scala.concurrent.{ ExecutionContext, Future }

class PhotoService(photoRepository: PhotoRepository) {

  def upload(username: String, description: String, md5: String, file: Source[ByteString, _], contentLength: Long)(implicit ec: ExecutionContext): Future[Either[Failure, Photo]] = {
    photoRepository.save(
      md5 = md5,
      username = username,
      description = description,
      photo = file,
      contentLength = contentLength
    )
  }

  def getInfo(id: String)(implicit ec: ExecutionContext): Future[Option[Photo]] = photoRepository.findById(id)

}
