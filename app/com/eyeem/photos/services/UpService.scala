package com.eyeem.photos.services

import scala.concurrent.Future

class UpService {

  def up(): Future[String] = {
    Future.successful("UP")
  }

}
