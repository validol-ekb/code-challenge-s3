package com.eyeem.photos.controllers

import java.io.File
import java.security.MessageDigest

import akka.stream.scaladsl.{ FileIO, Flow, Keep }
import akka.stream.stage.{ GraphStageLogic, GraphStageWithMaterializedValue, InHandler, OutHandler }
import akka.stream.{ Attributes, FlowShape, Inlet, Outlet }
import akka.util.ByteString
import org.apache.commons.codec.binary.Hex
import play.api.libs.Files.TemporaryFileCreator
import play.api.libs.streams.Accumulator
import play.api.mvc.MultipartFormData.FilePart
import play.core.parsers.Multipart
import play.core.parsers.Multipart.FileInfo

import scala.concurrent.{ ExecutionContext, Future, Promise }

/**
 * https://www.playframework.com/documentation/2.6.x/ScalaFileUpload#Writing-your-own-body-parser
 */
object FilePartHandlerWithMd5 {

  case class FileWithMetadata(file: File, md5: String, size: Long)

  def apply(temporaryFileCreator: TemporaryFileCreator)(implicit ec: ExecutionContext): Multipart.FilePartHandler[FileWithMetadata] = {
    case FileInfo(partName, filename, contentType) =>
      val file = temporaryFileCreator.create("requestBody", "asTemporaryFile")
      val sink = Flow[ByteString]
        .viaMat(DigestCalculator("MD5"))(Keep.right)
        .toMat(FileIO.toPath(file.path))(Keep.both).mapMaterializedValue(materialized =>
          for {
            hash <- materialized._1
            ioresult <- materialized._2
          } yield (hash, ioresult))

      val accumulator = Accumulator(sink)

      accumulator.map {
        case (hash, io) => FilePart(partName, filename, contentType, FileWithMetadata(file = file, md5 = hash, size = io.count))
      }
  }

  object DigestCalculator {
    def apply(algorhitm: String) = new DigestCalculator(algorhitm)
  }

  /**
   * This flow calculates the hash of the content that flows through it. It will emit the hash as its materialized value.
   * @param algorithm
   */
  class DigestCalculator(algorithm: String) extends GraphStageWithMaterializedValue[FlowShape[ByteString, ByteString], Future[String]] {

    val in = Inlet[ByteString]("DigestCalculator.in")
    val out = Outlet[ByteString]("DigestCalculator.out")

    val shape = FlowShape.of(in, out)

    override def createLogicAndMaterializedValue(inheritedAttributes: Attributes): (GraphStageLogic, Future[String]) = {
      val digest = MessageDigest.getInstance(algorithm)
      val promise = Promise[String]()
      val logic = new GraphStageLogic(shape) {

        setHandler(in, new InHandler {
          override def onPush(): Unit = {
            val elem = grab(in)
            digest.update(elem.toArray)
            push(out, elem)
          }

          override def onUpstreamFinish(): Unit = {
            super.onUpstreamFinish()
            promise.success(Hex.encodeHexString(digest.digest()))
          }
        })

        setHandler(out, new OutHandler {
          override def onPull(): Unit = {
            pull(in)
          }
        })

      }

      (logic, promise.future)
    }

  }

}
