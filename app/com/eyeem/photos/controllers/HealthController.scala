package com.eyeem.photos.controllers

import com.eyeem.photos.services.UpService
import play.api.libs.json.Json
import play.api.mvc.{ Action, AnyContent, BaseController, ControllerComponents }

import scala.concurrent.ExecutionContext

class HealthController(upService: UpService, val controllerComponents: ControllerComponents)(implicit ec: ExecutionContext) extends BaseController {

  def health: Action[AnyContent] = Action.async {
    upService.up().map {
      case "UP" => Ok(status("UP"))
      case _ => ServiceUnavailable(status("DOWN"))
    }
  }

  private def status(state: String) = Json.obj(
    "status" -> state
  )

}
