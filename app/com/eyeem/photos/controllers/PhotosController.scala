package com.eyeem.photos.controllers

import java.nio.file.Path

import akka.stream.scaladsl.FileIO
import com.eyeem.photos.NamedLogger
import com.eyeem.photos.controllers.FilePartHandlerWithMd5.FileWithMetadata
import com.eyeem.photos.controllers.PhotosController.{ ErrorResponse, UploadPhotoRequest }
import com.eyeem.photos.repositories.PhotoRepository.AlreadyExists
import com.eyeem.photos.services.{ ExifService, PhotoService }
import play.api.libs.Files.TemporaryFileCreator
import play.api.libs.json._
import play.api.mvc._

import scala.concurrent.ExecutionContext
import scala.util.Try

object PhotosController {
  case class UploadPhotoRequest(md5: String, username: String, description: String, contentLength: Long, tempFileRef: Path)
  object ErrorResponse {
    implicit val errorJsonWrites: Writes[ErrorResponse] = Json.writes[ErrorResponse]
  }
  case class ErrorResponse(status: Int, message: String)
}

class PhotosController(photoService: PhotoService, exifService: ExifService, temporaryFileCreator: TemporaryFileCreator, val controllerComponents: ControllerComponents)(implicit ec: ExecutionContext) extends BaseController with NamedLogger {

  def postPhoto: Action[UploadPhotoRequest] = Action.async(parseAndValidateRequest) { validRequest =>
    photoService.upload(
      username = validRequest.body.username,
      description = validRequest.body.description,
      md5 = validRequest.body.md5,
      contentLength = validRequest.body.contentLength,
      file = FileIO.fromPath(validRequest.body.tempFileRef)
    ) map {
        case Right(p) =>
          Created(Json.toJson(p))
        case Left(AlreadyExists(id)) =>
          Conflict(Json.toJson(ErrorResponse(status = 409, message = s"image with id=$id already exists")))
      } recover {
        case e =>
          internalServerError(e)
      }
  }

  def getPhoto(id: String) = Action.async {
    val photoF = photoService.getInfo(id)
    val exifF = exifService.getInfo(id)
    (for {
      photo <- photoF
      exif <- exifF
    } yield {
      photo match {
        case Some(v) =>
          Ok(Json.toJson(v.copy(exif = exif)))
        case _ =>
          NotFound
      }
    }).recover {
      case e =>
        internalServerError(e)
    }
  }

  private def parseAndValidateRequest = parse.using { request =>
    parse.multipartFormData(FilePartHandlerWithMd5(temporaryFileCreator)).validate(r => validate(r))
  }

  private def validate(body: MultipartFormData[FileWithMetadata]): Either[Result, UploadPhotoRequest] = for {
    photo <- body.file("photo") match {
      case Some(p) if p.ref.size > 0 => Right(p)
      case _ => Left(badRequest("no valid file provided"))
    }
    json <- body.dataParts("json").headOption.flatMap(js => Try(Json.parse(js)).toOption) match {
      case Some(json) => Right(json)
      case _ => Left(badRequest("no json provided"))
    }
    username <- (json \ "username").validate[String].asEither.left.map(_ => badRequest("no username provided"))
    description <- (json \ "description").validate[String].asEither.left.map(_ => badRequest("no description provided"))
  } yield UploadPhotoRequest(
    md5 = photo.ref.md5,
    username = username,
    description = description,
    contentLength = photo.ref.size,
    tempFileRef = photo.ref.file.toPath
  )

  private def internalServerError(exception: Throwable) = {
    log.error("Could not process request", exception)
    InternalServerError(Json.toJson(ErrorResponse(
      status = 500,
      message = exception.getMessage
    )))
  }

  private def badRequest(message: String) = BadRequest(Json.toJson(ErrorResponse(
    status = 400,
    message = message
  )))

}

