package com.eyeem.photos.repositories

import akka.http.scaladsl.model.{ ContentType, ContentTypes }
import akka.stream.alpakka.s3.impl.{ S3Headers, ServerSideEncryption }
import akka.stream.alpakka.s3.scaladsl.{ ObjectMetadata, S3Client }
import akka.stream.scaladsl.Source
import akka.util.ByteString
import com.eyeem.photos.NamedLogger
import com.eyeem.photos.model.Photo
import com.eyeem.photos.repositories.PhotoRepository.{ AlreadyExists, Failure }
import com.eyeem.photos.services.HealthIndicating

import scala.concurrent.{ ExecutionContext, Future }

object PhotoRepository {
  sealed trait Failure
  case class AlreadyExists(id: String) extends Failure
}
trait PhotoRepository extends HealthIndicating {

  /**
   * Tries to save the photo into a repository.
   * If the photo already exists, it will return a Failure, otherwise the Photo
   * @param md5
   * @param username
   * @param description
   * @param photo
   * @return
   */
  def save(md5: String, username: String, description: String, photo: Source[ByteString, _], contentLength: Long)(implicit ec: ExecutionContext): Future[Either[Failure, Photo]]

  /**
   * Finds Some photo or None photo
   * @param id
   * @return
   */
  def findById(id: String)(implicit ec: ExecutionContext): Future[Option[Photo]]

}

/**
 * I exist, because the guys from alpakka decided to make the S3Client final and I cant get PowerMock or this mockito2 extension stuff working :(
 * I dont want to loose my hairs over this for now.
 * TODO: Watch https://github.com/akka/alpakka/issues/1127
 */
class S3ClientWrapper(s3Client: S3Client) {
  def listBucket(
    bucket: String,
    prefix: Option[String]
  ) = s3Client.listBucket(bucket, prefix)
  def getObjectMetadata(
    bucket: String,
    key: String,
    versionId: Option[String] = None,
    sse: Option[ServerSideEncryption] = None
  ): Future[Option[ObjectMetadata]] = s3Client.getObjectMetadata(bucket, key, versionId, sse)

  def putObject(
    bucket: String,
    key: String,
    data: Source[ByteString, _],
    contentLength: Long,
    contentType: ContentType = ContentTypes.`application/octet-stream`,
    s3Headers: S3Headers,
    sse: Option[ServerSideEncryption] = None
  ): Future[ObjectMetadata] = s3Client.putObject(bucket, key, data, contentLength, contentType, s3Headers, sse)

}
class S3PhotoRepository(s3client: S3ClientWrapper, bucket: String, folder: String, region: String) extends PhotoRepository with NamedLogger {
  /**
   * Tries to save the photo into a repository.
   * If the photo already exists, it will return a Failure, otherwise the Photo
   *
   * @param md5
   * @param username
   * @param description
   * @param photo
   * @return
   */
  override def save(md5: String, username: String, description: String, photo: Source[ByteString, _], contentLength: Long)(implicit ec: ExecutionContext): Future[Either[Failure, Photo]] = {
    log.info(s"Trying to save photo with id=$md5 into folder=$folder")
    val key = ObjectKey(folder = folder, id = md5)
    s3client.getObjectMetadata(bucket = bucket, key = key).flatMap {
      case Some(_) =>
        log.info(s"Photo with id=$md5 already exists in folder=$folder")
        Future.successful(Left(AlreadyExists(md5)))
      case None =>
        s3client.putObject(
          bucket = bucket,
          key = key,
          data = photo,
          contentLength = contentLength,
          s3Headers = S3Headers(
            customHeaders = Map(
              CustomHeader("username") -> username,
              CustomHeader("description") -> description
            )
          )
        ).map(_ => {
            log.info(s"Successfully uploaded photo with id=$md5 to folder=$folder ")
            Right(Photo(
              id = md5,
              description = description,
              username = username,
              url = Some(UrlMaker(bucket = bucket, region = region, folder = folder, id = md5))
            ))
          })
    }
  }

  /**
   * Finds Some photo or None photo
   *
   * @param id
   * @return
   */
  override def findById(id: String)(implicit ec: ExecutionContext): Future[Option[Photo]] = {
    log.info(s"Looking for photo with id=$id in folder=$folder in bucket=$bucket in region=$region")
    s3client.getObjectMetadata(
      bucket = bucket,
      key = ObjectKey(folder = folder, id = id)
    ).map {
        case Some(metadata) => Some(Photo(
          id = id,
          description = metadata.metadata.find(_.name() == CustomHeader("description")).map(_.value()).getOrElse("lets assume our data is valid"),
          username = metadata.metadata.find(_.name() == CustomHeader("username")).map(_.value()).getOrElse("lets assume our data is valid"),
          url = Some(UrlMaker(bucket = bucket, region = region, folder = folder, id = id))
        ))
        case _ => None
      }
  }

  /**
   *
   * @return true => healthy
   */
  override def healthy()(implicit ec: ExecutionContext): Future[Boolean] = {
    log.info(s"Checking connectivity to s3 bucket=$bucket, folder=$folder...")
    s3client.getObjectMetadata(bucket = bucket, key = ObjectKey(folder = folder, id = "ItDoesntReallyMatter_HauptsacheAuthenticatedResponse"))
      .map(_ => true)
      .recover {
        case exception =>
          log.error(s"Could not connect to bucket=$bucket, folder=$folder, feeling unhealthy", exception)
          false
      }
  }

}
protected object CustomHeader {
  def apply(name: String) = s"x-amz-meta-$name"
}
protected object ObjectKey {
  def apply(folder: String, id: String) = s"$folder/$id.jpg"
}
protected object UrlMaker {
  def apply(bucket: String, region: String, folder: String, id: String) = s"http://$bucket.s3-website-$region.amazonaws.com/$folder/$id.jpg"
}

